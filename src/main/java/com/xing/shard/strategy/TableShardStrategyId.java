package com.xing.shard.strategy;

import cn.hutool.core.util.StrUtil;
import com.xing.shard.interceptor.ITableShardStrategy;
import org.springframework.stereotype.Component;

/**
 * @Author 程序猿阿星
 * @Description 分表策略id
 * @Date 2021/5/9
 */
@Component
public class TableShardStrategyId implements ITableShardStrategy {
    @Override
    public String generateTableName(String tableNamePrefix, Object value) {
        verificationTableNamePrefix(tableNamePrefix);
        if (value == null || StrUtil.isBlank(value.toString())) {
            throw new RuntimeException("value is null");
        }
        long id = Long.parseLong(value.toString());
        return tableNamePrefix + "_" + (id % 2);
    }
}
