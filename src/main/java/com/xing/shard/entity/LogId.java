package com.xing.shard.entity;

import java.util.Date;

/**
 * @Author 程序猿阿星
 * @Description  id分表日志
 * @Date 2021/5/8
 */
public class LogId {
    private long id;
    private String comment;
    private Date createDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
